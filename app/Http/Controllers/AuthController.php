<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('register');
    }

    
    public function welcome_post(Request $request){
        //dd($request->all());
        $fname = $request->firstname;
        $lname = $request->lastname;
        return view('welcome',compact('fname','lname'));

    }
}
