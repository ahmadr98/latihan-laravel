<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
</head>
<body>
<h1>Buat Account Baru !</h1>
    <h3>Sign Up Form</h3>
    <!--Form-->
    <form action="/welcome" method="POST">
    @csrf
        <label>First name:</label><br><br>
        <input type="text" name="firstname" ><br><br>
        <label>Last name:</label><br><br>
        <input type="text" name="lastname" ><br><br>
    <!--Gender-->
        <label>Gender :</label><br><br>
        <input type="radio" name=male> Male <br>
        <input type="radio" name=male> Female <br>
        <input type="radio" name=male> Other <br><br>
    <!--Nationality-->
        <label>Nationality :</label><br><br>
        <select name="national">
            <option value="Indonesian"> Indonesian</option>
            <option value="Singapur"> Singapur</option>
            <option value="Malaysian"> Malaysian</option>
            <option value="Australian"> Australian</option>
        </select>
        <br><br>

    <!--Languange-->
        <label>Languange Spoken :</label><br><br>
        <input type="checkbox"> Bahasa Indonesia<br>
        <input type="checkbox"> English <br>
        <input type="checkbox"> Arabic <br>
        <input type="checkbox"> Japanese <br>
        <input type="checkbox"> Other <br><br>

    <!--Bio-->
    <label>Bio :</label><br><br>   
    <textarea name="bio" cols="30" rows="10"></textarea> <br>

    <!--SignUp-->
    <input type="submit" value="Sign Up">

      </form> 
</body>
</html>